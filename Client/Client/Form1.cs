﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net;
using System.Net.Sockets;
using System.IO;
using System.Threading;
using System.Diagnostics;
using System.Runtime.InteropServices;
using gma.System.Windows;

namespace Client
{
    public partial class Form1 : Form
    {
        static private string DELIMITER = ";;;S";
        static private int DEFAULT_BUFLEN = 256;

        static private Socket Client;
        private IPAddress ip = null;
        private int port = 7770;
        private bool is_disconected = false;
        private static int my_id = -1;
        private Thread th;
        private Thread th2;

        static protected int currX;
        static protected int currY;

        static private bool is_keyboard = false;
        static private bool is_mouse = false;
        static private int im_not_active_times = 0;

        public Form1()
        {
            InitializeComponent();
            try
            {
                var sr = new StreamReader(@"ClientInfo/data_info.txt");
                string buffer = sr.ReadToEnd();
                sr.Close();
                ip = IPAddress.Parse(buffer.Trim());
                SetInfo(Color.Green, "Подключение к " + ip.ToString() + ":" + port.ToString());
                labelName.Text = "Имя компьютера: " + Environment.MachineName;
                init();
            }
            catch (Exception ex)
            {
                SetInfo(Color.Red, ex.Message);
                ShowSett();
            }
        }

        void init()
        {
            Client = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            if (ip != null)
            {
                Client.Connect(ip, port);
                th = new Thread(delegate () { SendActive(); }); th.Start();
                th2 = new Thread(delegate () { RecvMessage(); }); th2.Start();
                sendName();
                this.Load += new EventHandler(this.MainFormLoad);
            }
        }

        void ShowSett()
        {
            Settings s = new Settings();
            s.Show();
        }

        void SetInfo(Color cl, string text){
            infoPanel.ForeColor = cl;
            infoPanel.Text = text;
        }

        void Log(string mes)
        {
            Debug.WriteLine(mes);
        }

        void sendName(){
            SendMessage(Environment.MachineName);
        }

        void SendMessage(string message)
        {
            if (message.Trim() == "")
                return;
            if(Client.Connected){
                byte[] buffer = new byte[DEFAULT_BUFLEN];
                buffer = Encoding.UTF8.GetBytes(message + DELIMITER);
                Client.Send(buffer);
            }
        }

        void RecvMessage()
        {
            byte[] buffer = new byte[1024];
            for (int i = 0; i < buffer.Length; i++) buffer[i] = 0;

            for (; ; )
            {
                try
                {
                    if (Client.Connected){
                        Client.Receive(buffer);
                        string message = Encoding.UTF8.GetString(buffer);
                        int count = message.IndexOf(DELIMITER);
                        if (count == -1) continue;

                        string clear_message = "";

                        for (int i = 0; i < count; i++) clear_message += message[i];
                        for (int i = 0; i < buffer.Length; i++) buffer[i] = 0;

                        this.Invoke((MethodInvoker) delegate (){
                            if(clear_message == "get_name") sendName();
                        });
                    } else {
                        if(!is_disconected){
                            is_disconected = !is_disconected;
                            SetDisconected();
                            break;
                        }
                    }

                }
                catch (Exception ex)
                {

                }
            }
        }

        void SendActive()
        {
            for (; ; )
            {
                try
                {
                    if (Client.Connected){
                        this.Invoke((MethodInvoker) delegate (){
                            SendMessage(check_the().ToString());
                        });
                        Thread.Sleep(100);
                    } else {
                        if(!is_disconected){
                            is_disconected = !is_disconected;
                            SetDisconected();
                            break;
                        }
                    }
                }
                catch (Exception ex)
                {
                    Log(ex.Message);
                }
            }
        }

        void SetDisconected()
        {
            Log("SetDisconected");
            SetInfo(Color.Red, "Disconected");
        }

        private void vtyToolStripMenuItem_Click(object sender, EventArgs e)
        {
        }

        private void настройкиToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ShowSett();
        }

        private void авторToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show("By Umbokc.\n2018");
        }

        private void выходToolStripMenuItem_Click(object sender, EventArgs e)
        {
            BeforeClose();
            Application.Exit();
        }
        
        private void BeforeClose()
        {
            if (th != null) th.Abort();
            if (th2 != null) th2.Abort();
            if (Client != null) Client.Close();
            timer1.Stop();
        }
  
        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            BeforeClose();
        }

        private void getid_Click(object sender, EventArgs e)
        {
        }

        void check_cursor(){
            var pos = Cursor.Position;

            if (currX != pos.X && currY != pos.Y)
            {
                im_not_active_times = 0;
                is_mouse = true;
            } else {
                is_mouse = false;
            }

            currX = pos.X;
            currY = pos.Y;
        }

        int check_the(){
            check_cursor();

            bool the_asd_asd_active = false;

            if(is_keyboard || is_mouse){
                the_asd_asd_active = true;
                is_keyboard = false;
                is_mouse = false;
            }

            if(!the_asd_asd_active){
                if(im_not_active_times < 30){
                    im_not_active_times++;
                    the_asd_asd_active = true;
                }
            }

            if(the_asd_asd_active){
                if(label_coord.Text != "Active")
                    label_coord.Text = "Active";
                return 1;
            } else {
                if(label_coord.Text != "UnActive")
                    label_coord.Text = "UnActive";
                return 0;
            }
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
        }

        void set_keyboard_active_true(){
            is_keyboard = true;
            im_not_active_times = 0;
        }

        UserActivityHook actHook;
        void MainFormLoad(object sender, System.EventArgs e)
        {
            actHook = new UserActivityHook();
            actHook.OnMouseActivity+=new MouseEventHandler(MouseMoved);
            actHook.KeyDown+=new KeyEventHandler(MyKeyDown);
            actHook.KeyPress+=new KeyPressEventHandler(MyKeyPress);
            actHook.KeyUp+=new KeyEventHandler(MyKeyUp);
        }
        
        public void MouseMoved(object sender, MouseEventArgs e)
        {
            if (e.Clicks>0){
                set_keyboard_active_true();
            }
        }
        
        public void MyKeyDown(object sender, KeyEventArgs e)
        {
            set_keyboard_active_true();
        }
        
        public void MyKeyPress(object sender, KeyPressEventArgs e)
        {
            set_keyboard_active_true();
        }
        
        public void MyKeyUp(object sender, KeyEventArgs e)
        {
            set_keyboard_active_true();
        }

    }
}
