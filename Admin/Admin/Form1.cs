﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net;
using System.Net.Sockets;
using System.IO;
using System.Threading;

namespace Admin
{
    public partial class Form1 : Form
    {
        static private string delimiter = ";;;S";
        static private int DEFAULT_BUFLEN = 256;

        static private Socket Client;
        private IPAddress ip = null;
        private int port = 7770;
        private bool is_disconected = false;
        private Thread th;

        static int active_times = 0;
        static int not_active_times = 0;

        ListViewItem lv;
   
        public Form1()
        {
            InitializeComponent();
            try
            {
                var sr = new StreamReader(@"ClientInfo/data_info.txt");
                string buffer = sr.ReadToEnd();
                sr.Close();
                ip = IPAddress.Parse(buffer.Trim());
                info.ForeColor = Color.Green;
                info.Text = "Подключено к " + ip.ToString() + ":" + port.ToString();
            }
            catch (Exception ex)
            {
                info.ForeColor = Color.Red;
                info.Text = "Настройки не найдены";
                ShowSett();
            }
            
            try
            {
                init();
            }
            catch (Exception ex)
            {
                info.ForeColor = Color.Red;
                info.Text = ex.Message;
            }
        }

        void init()
        {
            Client = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            if (ip != null)
            {
                Client.Connect(ip, port);
                th = new Thread(delegate () { RecvMessage(); });
                th.Start();
                SendMessage("admin");
            }
        }

        string iw_genNam(string name){
            return name;
        }

        bool iw_has(string name){
            for (int n = listView1.Items.Count - 1; n >= 0; --n)
            {
                if (listView1.Items[n].SubItems[0].Text == iw_genNam(name))
                {
                    return true;
                }
            }
            return false;
        }

        void iw_add(string name){
            //listBoxItems.Items.Add(iw_genNam(name));
            lv = new ListViewItem(iw_genNam(name));
            lv.SubItems.Add("0");
            lv.SubItems.Add("0");
            listView1.Items.Add(lv);
        }

        int iw_get_id(string name){
           for (int n = listView1.Items.Count - 1; n >= 0; --n)
           {
                if (listView1.Items[n].SubItems[0].Text == iw_genNam(name))
                {
                    return n;
                }
            }
            return -1;
        }

        void iw_rem(string name){
            iw_remAt(iw_get_id(name));
        }

        void iw_remAt(int index){
           listView1.Items.RemoveAt(index);
        }

        void iw_incr_active(string name){
            not_active_times = 0;
            if(active_times >= 10){
                active_times = 0;
                int n = iw_get_id(name);
                int val = int.Parse(listView1.Items[n].SubItems[1].Text);
                val++;
                listView1.Items[n].SubItems[1].Text = val.ToString();
            } else {
                active_times++;
            }
        }

        void iw_incr_not_active(string name){
            active_times = 0;
            if(not_active_times >= 10){
                not_active_times = 0;
                int n = iw_get_id(name);
                int val = int.Parse(listView1.Items[n].SubItems[2].Text);
                val++;
                listView1.Items[n].SubItems[2].Text = val.ToString();
            } else {
                not_active_times++;
            }
        }

        void ShowSett()
        {
            Settings s = new Settings();
            s.Show();
        }

        void executeComand(string mes)
        {

            string[] data = mes.Split('|');

            if(data.Length <= 1) return;
      
            string name = data[0];
            string val = data[1];

            if(val == "close"){
                if(iw_has(name))
                    iw_rem(name);
                return;
            }

            if(!iw_has(name)){
                iw_add(name);
            } else {
                if(val == "1"){
                    iw_incr_active(name);
                } else if(val == "0"){
                    iw_incr_not_active(name);
                }
            }

        }

        void SendMessage(string message)
        {
            if (message.Trim() == "")
                return;

            byte[] buffer = new byte[DEFAULT_BUFLEN];
            buffer = Encoding.UTF8.GetBytes(message + delimiter);
            Client.Send(buffer);
        }

        void SetDisconected()
        {
            Log("SetDisconected");
            info.ForeColor = Color.Red;
            info.Text = "Disconected";
        }

        void RecvMessage()
        {
            byte[] buffer = new byte[DEFAULT_BUFLEN];

            for (int i = 0; i < buffer.Length; i++)
            {
                buffer[i] = 0;
            }

            for (; ; )
            {
                try
                {
                    if (Client.Connected){
                        Client.Receive(buffer);
                        string message = Encoding.UTF8.GetString(buffer);
                        int count = message.IndexOf(delimiter);
                        if (count == -1)
                        {
                            continue;
                        }
                        string clear_message = "";

                        for (int i = 0; i < count; i++)
                        {
                            clear_message += message[i];
                        }
                        for (int i = 0; i < buffer.Length; i++)
                        {
                            buffer[i] = 0;
                        }

                        this.Invoke((MethodInvoker)delegate ()
                        {
                           executeComand(clear_message);
                        });
                    } else {
                        if(!is_disconected){
                            is_disconected = !is_disconected;
                            SetDisconected();
                            break;
                        }
                    }

                }
                catch (Exception ex)
                {

                }
            }
        }

        void Log(string mes)
        {
            System.Diagnostics.Debug.WriteLine(mes.ToString());
        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void авторToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show("By Umbokc.\n 2018");
        }

        private void настройкиToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ShowSett();
        }

        private void выходToolStripMenuItem_Click(object sender, EventArgs e)
        {
            BeforeClose();
            Application.Exit();
        }

        private void BeforeClose()
        {
            if (th != null) th.Abort();
            if (Client != null) Client.Close();
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            BeforeClose();
        }

        private void listBoxItems_SelectedIndexChanged(object sender, EventArgs e)
        {
            
        }

        private void listBoxItems_SelectedValueChanged(object sender, EventArgs e)
        {
            
        }
    }
}
